package main

import (
	"flag"
	"net/http"
	"fmt"
	"log"
	"io/ioutil"
	"time"
	"encoding/json"
	"math"
)

type ServicePrice struct {
	Price	int	`json:"price"`
	Source 	string	`json:"source"`
}

type Price struct {
	Price    int    `json:"price"`
}


func main() {
	port := flag.String("port", ":7878", "port")
	flag.Parse()

	http.HandleFunc("/winner", Handle)

	fmt.Printf("Listen on %s\n", *port)
	log.Fatal(http.ListenAndServe(*port, nil))
}


func Handle(writer http.ResponseWriter, request *http.Request) {
	urls := request.URL.Query()["s"]

	ch := make(chan ServicePrice, 1)
	for _, url := range urls {
		go makeRequest(url, ch)
	}

	minPrice := ServicePrice{math.MinInt64, "no-url"}
	first, second := minPrice, minPrice
	response, _ := json.Marshal(second)
	loop:
		for {
			select {
				case ret := <-ch:
					if ret.Price > first.Price {
						second = first
						first = ret
					} else {
						if ret.Price > second.Price && ret.Price != first.Price {
							second = ret
						}
					}
					response, _ = json.Marshal(second)
				case <-time.After(100 * time.Millisecond):
					log.Println("Timed out")
					break loop
				}
		}

	if second == minPrice {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	writer.Write(response)
}


func makeRequest(url string, ch chan<- ServicePrice) {
	const timeout = time.Duration(100 * time.Millisecond)
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(url)

	if err != nil {
		log.Println("Error: ", err)
	} else {
		defer resp.Body.Close()
		if (resp.StatusCode == http.StatusOK) {
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println("Response error: ", err)
			} else {
				var prs []Price
				errJson := json.Unmarshal(body, &prs)
				if errJson != nil {
					log.Println("Json deconding error: ", err)
				}
				for _, price := range prs {
					ch <- ServicePrice{price.Price, url}
				}
			}
		}
	}
}

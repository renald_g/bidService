### Running

```
$ go run main.go [-port=:8080]
```

### Test

```
$ go test service_test.go main.go -cpu 1
```
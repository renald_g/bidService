package main

import (
	"testing"
	"net/http"
	"net/http/httptest"
	"time"
	"encoding/json"
	"math/rand"
	"fmt"
)

type row struct {
	Price int `json:"price"`
}

func testServices(weights []int, timeout int) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, _ *http.Request) {
		lat := timeout
		time.Sleep(time.Duration(lat) * time.Millisecond)

		x := rand.Intn(100)
		if x < 10 {
			http.Error(w, "service unavailable", http.StatusServiceUnavailable)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		// Randomisation
		r := rand.New(rand.NewSource(time.Now().Unix()))

		resp := make([]row, 0, len(weights))
		for _, k := range r.Perm(len(weights)) {
			resp = append(resp, row{weights[k]})
		}

		json.NewEncoder(w).Encode(resp)
	}
}


func TestHandlerPositive(t *testing.T) {
	ts1 := httptest.NewServer( http.HandlerFunc(testServices([]int{2, 3, 5, 7, 11, 13, 17, 19, 23}, 10)))
	defer ts1.Close()

	ts2 := httptest.NewServer( http.HandlerFunc(testServices([]int{1, 2, 6, 24}, 10)))
	defer ts2.Close()

	ts3 := httptest.NewServer( http.HandlerFunc(testServices([]int{29, 31}, 1000)))
	defer ts3.Close()


	req, err := http.NewRequest("GET", "/winner", nil)
	if err != nil {
		t.Fatal(err)
	}
	q := req.URL.Query()
	q.Add("s", ts1.URL)
	q.Add("s", ts2.URL)
	q.Add("s", ts3.URL)
	req.URL.RawQuery = q.Encode()

	handler := http.HandlerFunc(Handle)
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := fmt.Sprintf(`{"price":23,"source":"%s"}`, ts1.URL)
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
